
canlock-hp
==========
Header parsers for libcanlock (a library for creating and verifying RFC 8315
Netnews Cancel-Locks).


1) General
libcanlock contains no header parser because some programs already have one.
If your program uses libcanlock (e.g. via canlock frontend in a script) and has
no header parser, the utilities in this package can be used to fill the gap.


2) System requirements
- POSIX.1-2001 conformant operating system
- POSIX C-Language Development Utilities (c99, lex, yacc)

flex and bison are automatically detected as replacements for lex and yacc.


3) Versioning scheme
Formerly canlock-hp was a separate package with own version number.
The canlock-hp utilities are merged into the libcanlock package since 3.1.0 and
now use the same version number.


canlock-mhp
===========
Message Header Parser (mhp) for RFC 5536 conformant Netnews articles.

The name of a single header field must be specified with the -f option.
The message in canonical format (with US-ASCII encoded CRLF line breaks) is
read from stdin. If the message is in some local format (e.g. EBCDIC encoded
or with LF line breaks), it must be converted to canonical format before it
can be used as input.
If the specified header field is found, it is extracted, unfolded and printed
to stdout.

Note:
The input data may be a naked header without message body, but the empty line
separator after the header must always be present.
The syntax of the extracted header field is not checked beyond the folding.
The rest of the message need not be fully RFC 5536 conformant too, as long as
all header fields start on new lines (and are correctly folded, so that there
is no ambiguity). Common syntax errors (like 8bit characters in the header)
are ignored.


canlock-hfp
===========
Header Field Parser (hfp) for RFC 8315 conformant header fields.

The complete unfolded header field (with its name at the beginning and without
line break at the end) is read from stdin and strictly parsed for the syntax
defined in RFC 8315 for the Cancel-Lock and Cancel-Key header fields.
The c-lock or c-key elements (respectively) are printed to stdout, separated
with spaces (usable as list of "word"s in the "for" loop of a POSIX shell).

Note:
Additional notes, line breaks between the elements and potential comments from
the header field are printed to stderr to make the output more human readable
on a terminal.


libcanlock-hp
=============
Since version 3.3.0 the library libcanlock-hp is available.
It contains header parser API functions for C programs.

The subdirectory "examples" contains programs with demonstration code.


EOF
