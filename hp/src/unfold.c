/* ========================================================================== */
/* Copyright (c) 2021 Michael Baeuerle
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 */

/*! Tell headers that they should be POSIX compliant */
#define _POSIX_C_SOURCE  200112L

/* C99 */
#include <stdlib.h>
#include <string.h>

/* Local */
#include "canlock-hp.h"


/* ========================================================================== */
/*! \brief Unfold header field
 *
 * \param[in]     data      Pointer to input data
 * \param[in,out] data_len  Pointer to length of input data
 *
 * The input data must be a single header field.
 * Potential folding will be removed by this function.
 *
 * RFC 5322 specifies in Section 2.2 that CR and LF control characters in header
 * field bodies are allowed for folding use only.
 * The algorithm used by this function removes all CR and LF control characters
 * from the input data.
 *
 * \note
 * A valid header field must start with a name and a colon.
 * It is treated as error if the length is zero or there is no colon present.
 *
 * \return
 * Upon successful completion a pointer to a new memory block, containing the
 * unfolded header field with NUL-termination, is returned.
 * Otherwise, \c NULL is returned.
 * <br>
 * On success the caller is responsible to call free(3) for the pointer
 * returned as result.
 * <br>
 * On success the value in \e data_len is updated to the length of the unfolded
 * header field (without NUL-termination).
 */
char *cl_hp_unfold_field(const char *data, size_t *data_len)
{
   char *buf = NULL;
   size_t i;
   size_t len = 0;
   int colon_present = 0;

   /* Zero length is not allowed */
   if (*data_len)
   {
      buf = (char *) malloc(*data_len + (size_t) 1);
      if (NULL != buf)
      {
         for (i = 0; *data_len > i; ++i)
         {
            if ('\r' != data[i] && '\n' != data[i])
            {
               if (':' == data[i])
                  colon_present = 1;
               buf[len++] = data[i];
            }
         }
         *data_len = len;

         /* Append NUL-termination */
         buf[len] = 0;
      }
   }

   /* Check whether colon was found */
   if (!colon_present)
   {
      free((void *) buf);
      buf = NULL;
   }

   return buf;
}


/* EOF */
