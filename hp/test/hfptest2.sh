# No shebang here is intended
# (use the shell that autotools configured for test-driver)

set -e

printf "%s\n\n" "Check <ctext> for comments harder"

RESULT=$(../util/canlock-hfp <<"EOL"
caNcel-LOCK: (!""#$%&''*+,-./12345: ;<=>?@ABC[]^_``abc{|}~\() ShA256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=(sha1:abc)(\\\)!""#$%&''*+,-./0-9:;<=>?@A-Z[]^_``a-z{|}~)sHa256:SrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=
EOL
)
printf "%s\n" "$RESULT"
if test "$RESULT" = "sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo= sha256:SrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo="
then
   exit 0
else
   printf "Error!\n"
   exit 1
fi


# EOF
