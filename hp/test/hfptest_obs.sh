# No shebang here is intended
# (use the shell that autotools configured for test-driver)

set -e

printf "%s\n\n" "Check with obsolete syntax"

RESULT=$(../util/canlock-hfp <<EOL
caNcel-KeY: 	 	(sha1:bca)sHa1:abc
EOL
)
if test "$RESULT" = "sha1:abc"
then
   printf "%s\n" "$RESULT"
   exit 0
else
   printf "Error!\n"
   exit 1
fi


# EOF
