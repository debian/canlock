.\"=============================================================================
.\" Header
.\"
.\" Copyright (c) 2021 Michael Baeuerle
.\"
.\" All rights reserved.
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, and/or sell copies of the Software, and to permit persons
.\" to whom the Software is furnished to do so, provided that the above
.\" copyright notice(s) and this permission notice appear in all copies of
.\" the Software and that both the above copyright notice(s) and this
.\" permission notice appear in supporting documentation.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
.\" OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
.\"  HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
.\" SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
.\" RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
.\" CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.\" Except as contained in this notice, the name of a copyright holder
.\" shall not be used in advertising or otherwise to promote the sale, use
.\" or other dealings in this Software without prior written authorization
.\" of the copyright holder.
.\"
.TH $NAME 3 $DATE Unix "$NAME $VERSION manual"
.\"
.\"
.\"=============================================================================
.\" NAME section
.\"
.SH NAME
cl_verify_multi - Check multiple keys against multiple locks with $NAME
.\"
.\"
.\"=============================================================================
.\" SYNOPSIS section
.\"
.SH SYNOPSIS
.nf
.B #include <libcanlock-3/canlock.h>
.sp
.BI "int cl_verify_multi(cl_hash_version *" rejected_hash_list ,
.BI "                    const char *" c_key_list ,
.BI "                    const char *" c_lock_list );
.fi
.sp
Supported values for
.I reject_hash
(by version 3.3.0 of $NAME):
.br
CL_SHA1 CL_SHA224 CL_SHA256 CL_SHA384 CL_SHA512
.\"
.\"
.\"=============================================================================
.\" DESCRIPTION section
.\"
.SH DESCRIPTION
Attention: This function is not available with versions before 3.3.0.
.sp
The
.BR cl_verify_multi ()
function checks a list of <c-key> elements against a list of <c-lock> elements
according to the algorithm defined by RFC 8315 in Section 3.5.
.sp
Hash algorithms for <scheme> that should be rejected can be specified with
.IR "rejected_hash_list" .
Specify either NULL, to accept all hash algorithms supported by $NAME, or
a pointer to an array terminated with CL_INVALID.
.br
A reject list is used instead of an accept list so that programs using $NAME
automatically inherit support for stronger hash algorithms that may be added
in the future.
.sp
The <c-key> elements must be specified with
.I c_key_list
and the and <c-lock> elements with
.IR "c_lock_list" .
Both must be strings where the elements are separated by single SP (space)
characters.
.br
The output of the header field parser
.BR canlock-hfp
is suitable for
.I c_key_list
and
.IR "c_lock_list" .
The function
.BR cl_hp_parse_field ()
from the libcanlock-hp library can be used to execute the parser.
.\"
.\"
.\"=============================================================================
.\" RETURN VALUE section
.\"
.SH RETURN VALUE
If one of the keys matches one of the locks, zero is returned.
.br
All other values indicate an error.
.\"
.\"
.\"=============================================================================
.\" AUTHORS section
.\"
.SH AUTHORS
Michael Baeuerle
.\"
.\"
.\"=============================================================================
.\" REPORTING BUGS section
.\"
.SH REPORTING BUGS
Report bugs to <mailto:$BUGREPORT>.
.\"
.\"
.\"=============================================================================
.\" STANDARDS section
.\"
.SH STANDARDS
.B $NAME
tries to comply with the following standards:
.PP
RFC 5537, RFC 6234, RFC 8315
.\"
.\"
.\"=============================================================================
.\" SEE ALSO section
.\"
.SH SEE ALSO
.BR cl_clear_secret (3),
.BR cl_get_key (3),
.BR cl_get_lock (3),
.BR cl_split (3),
.BR cl_verify (3),
.BR cl_hp_get_field(3),
.BR cl_hp_unfold_field(3),
.BR cl_hp_parse_field(3),
.BR canlock (1),
.BR canlock-hfp (1),
.BR canlock-mhp (1)
.\"
.\"
.\" EOF
