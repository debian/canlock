# No shebang here is intended
# (use the shell that autotools configured for test-driver)

set -e

printf "%s\n\n" "Check with current syntax"

RESULT=$(../util/canlock-hfp <<"EOL"
caNcel-LOCK: (Comment with quoted-pair: \(, (Not scheme sha1:)) ShA256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=(sha1:abc)
EOL
)
printf "%s\n" "$RESULT"
if test "$RESULT" = "sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo="
then
   exit 0
else
   printf "Error!\n"
   exit 1
fi


# EOF
