#! /bin/sh

# Authenticate a cancel or supersede article against a target article
# (the one that should be replaced)
#
# The name of the file with the cancel or supersede article must be passed
# as the first parameter, the name of the file with the target article as
# the second parameter. Both articles must be in canonical format (with
# CRLF line breaks).

set -e

# Check parameters
if test $# -ne 2
then
   printf "%s\n%s\n" "Usage example: $0 supersede.txt target.txt" \
          "(Both articles must be in canonical format)"
   exit 1
fi

# Check for utilities to be installed
canlock-mhp -v >/dev/null
canlock-hfp -v >/dev/null
canlock -v >/dev/null

# Extract keys (from the cancel or supersede article)
C_KEYS=$(<$1 canlock-mhp -f "Cancel-Key" | canlock-hfp 2>/dev/null)
printf "Keys : %s\n" "$C_KEYS" 1>&2

# Extract locks (from the target article)
C_LOCKS=$(<$2 canlock-mhp -f "Cancel-Lock" | canlock-hfp 2>/dev/null)
printf "Locks: %s\n" "$C_LOCKS" 1>&2

# Verify whether at least one key matches a lock
canlock -m "$C_KEYS","$C_LOCKS"
if test $? -ne 0
then
   exit 1
fi


# EOF
