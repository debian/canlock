/* Demonstration code that parses a Cancel-Lock header field with libcanlock-hp */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libcanlock-3/canlock-hp.h>

int main(void)
{
    /* Header field with nontrivial syntax */
    const char *input =
        "Cancel-Lock: (comment)"
        "	sHa256:s/pmK/3grrz++29ce2/mQydzJuc7iqHn1nqcJiQTPMc="
        "     (\\\\)	SHA256:NSBTz7BfcQFTCen+U4lQ0VS8VIlZao2b8mxD/xJaaeE="
        " (\\) sha1:nonsense)"
        " (comment)sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo="
        "          ShA1:bNXHc6ohSmeHaRHHW56BIWZJt+4=";
    const char *output;

    /* Parse for <c-lock> elements */
    output = cl_hp_parse_field(input, strlen(input));
    if (NULL == output)
    {
       printf("Parser failed.\n");
       exit(EXIT_FAILURE);
    }
    else
    {
       printf("Result: \n%s\n", output);
       free((void *) output);
       exit(EXIT_SUCCESS);
    }
}

/* EOF */
