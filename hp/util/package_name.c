/* ========================================================================== */
/* Copyright (c) 2018 Michael Baeuerle
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 */

/* C99 */
#include <stdlib.h>
#include <string.h>

/* GNU autoconf */
#include "config.h"

/* Local */
#include "package_name.h"


/* ========================================================================== */
/*! \brief Get package name
 *
 * \attention
 * On success the caller is responsible to free the memory allocated for the
 * result!
 *
 * \return
 * - Pointer to new memory block with result on success
 * - NULL on error
 */

const char *cl_hp_package_name(void)
{
   char *res = NULL;
   const char*  n = PACKAGE_NAME;
   char *p, *h;

   p = (char *) malloc(strlen(n) + (size_t) 1);
   if (NULL != p)
   {
      strcpy(p, n);
      h = strchr(p, (int) '-');
      if (NULL != h)
      {
         /* Strip at hyphen */
         *h = 0;
      }
      res = p;
   }

   return(res);
}


/* ========================================================================== */
/*! \brief Get package version
 *
 * \return
 * - Pointer to result on success
 * - NULL on error
 */

const char *cl_hp_package_version(void)
{
   return(PACKAGE_VERSION);
}


/* EOF */
