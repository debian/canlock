.\"=============================================================================
.\" Header
.\"
.\" Copyright (c) 2021 Michael Baeuerle
.\"
.\" All rights reserved.
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, and/or sell copies of the Software, and to permit persons
.\" to whom the Software is furnished to do so, provided that the above
.\" copyright notice(s) and this permission notice appear in all copies of
.\" the Software and that both the above copyright notice(s) and this
.\" permission notice appear in supporting documentation.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
.\" OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
.\"  HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
.\" SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
.\" RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
.\" CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.\" Except as contained in this notice, the name of a copyright holder
.\" shall not be used in advertising or otherwise to promote the sale, use
.\" or other dealings in this Software without prior written authorization
.\" of the copyright holder.
.\"
.TH $NAME 3 $DATE Unix "$PACKAGE $VERSION manual"
.\"
.\"
.\"=============================================================================
.\" NAME section
.\"
.SH NAME
$NAME - Execute field parser
.\"
.\"
.\"=============================================================================
.\" SYNOPSIS section
.\"
.SH SYNOPSIS
.nf
.B #include <libcanlock-3/canlock-hp.h>
.sp
.BI "char *cl_hp_parse_field(const char *" data ", size_t " data_len );
.\"
.\"
.\"=============================================================================
.\" DESCRIPTION section
.\"
.SH DESCRIPTION
The
.BR cl_hp_parse_field ()
function executes the header field parser.
.br
This implementation executes the
.BR canlock-hfp
utility in a separate process.
.sp
The parameter
.I data
should point to the beginning of the input data.
.br
Specify the length of the input data with the
.I data_len
parameter.
.sp
The input data must be an unfolded "Cancel-Lock" or "Cancel-Key" header field
without line break at the end.
.br
The function
.BR cl_hp_get_field ()
can be used to extract a field from the message header.
.br
The function
.BR cl_hp_unfold_field ()
can be used to remove potential folding from an already extracted header field.
.\"
.\"
.\"=============================================================================
.\" RETURN VALUE section
.\"
.SH RETURN VALUE
If the parser reports success, a pointer to the NUL-terminated output data is
returned. Otherwise,
.B NULL
is returned.
.sp
On success, the caller is responsible to
.BR free ()
the memory block allocated for the output data.
.\"
.\"
.\"=============================================================================
.\" AUTHORS section
.\"
.SH AUTHORS
Michael Baeuerle
.\"
.\"
.\"=============================================================================
.\" REPORTING BUGS section
.\"
.SH REPORTING BUGS
Report bugs to <mailto:$BUGREPORT>.
.\"
.\"
.\"=============================================================================
.\" STANDARDS section
.\"
.SH STANDARDS
.BR $NAME ()
tries to comply with the following standards:
.PP
RFC 8315
.\"
.\"
.\"=============================================================================
.\" SEE ALSO section
.\"
.SH SEE ALSO
.BR free (3),
.BR cl_hp_get_field (3),
.BR cl_hp_unfold_field (3),
.BR canlock-hfp (1),
.BR canlock-mhp (1),
.BR canlock (1)
.\"
.\"
.\" EOF
