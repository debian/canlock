.\"=============================================================================
.\" Header
.\"
.\" Copyright (c) 2021 Michael Baeuerle
.\"
.\" All rights reserved.
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, and/or sell copies of the Software, and to permit persons
.\" to whom the Software is furnished to do so, provided that the above
.\" copyright notice(s) and this permission notice appear in all copies of
.\" the Software and that both the above copyright notice(s) and this
.\" permission notice appear in supporting documentation.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
.\" OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
.\"  HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
.\" SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
.\" RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
.\" CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.\" Except as contained in this notice, the name of a copyright holder
.\" shall not be used in advertising or otherwise to promote the sale, use
.\" or other dealings in this Software without prior written authorization
.\" of the copyright holder.
.\"
.TH $NAME 3 $DATE Unix "$PACKAGE $VERSION manual"
.\"
.\"
.\"=============================================================================
.\" NAME section
.\"
.SH NAME
$NAME - Unfold header field
.\"
.\"
.\"=============================================================================
.\" SYNOPSIS section
.\"
.SH SYNOPSIS
.nf
.B #include <libcanlock-3/canlock-hp.h>
.sp
.BI "char *cl_hp_unfold_field(const char *" data ", size_t *" data_len );
.\"
.\"
.\"=============================================================================
.\" DESCRIPTION section
.\"
.SH DESCRIPTION
The
.BR cl_hp_unfold_field ()
function unfolds a header field.
.PP
The parameter
.I data
should point to the beginning of the input data.
.br
Specify the length of the input data with the value pointed to by the
.I data_len
parameter.
.PP
RFC 5322 specifies in Section 2.2 that CR and LF control characters in header
field bodies are allowed for folding use only.
This function removes all CR and LF control characters from the input data.
.\"
.\"
.\"=============================================================================
.\" RETURN VALUE section
.\"
.SH RETURN VALUE
Upon successful completion a pointer to a new memory block, containing the
unfolded header field with NUL-termination, is returned. Otherwise,
.B NULL
is returned.
.sp
On success, the caller is responsible to
.BR free ()
the memory block allocated for the output data.
.sp
On success the value pointed to by
.I data_len
is updated to the length of the unfolded header field (without NUL-termination).
.\"
.\"
.\"=============================================================================
.\" AUTHORS section
.\"
.SH AUTHORS
Michael Baeuerle
.\"
.\"
.\"=============================================================================
.\" REPORTING BUGS section
.\"
.SH REPORTING BUGS
Report bugs to <mailto:$BUGREPORT>.
.\"
.\"
.\"=============================================================================
.\" STANDARDS section
.\"
.SH STANDARDS
.BR $NAME ()
tries to comply with the following standards:
.PP
RFC 5322
.\"
.\"
.\"=============================================================================
.\" SEE ALSO section
.\"
.SH SEE ALSO
.BR free (3),
.BR cl_hp_get_field (3),
.BR cl_hp_parse_field (3),
.BR canlock-hfp (1),
.BR canlock-mhp (1),
.BR canlock (1)
.\"
.\"
.\" EOF
