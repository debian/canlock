/* =============================================================================
 * Copyright (c) 2021 Michael Baeuerle
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
 * SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder
 * shall not be used in advertising or otherwise to promote the sale, use
 * or other dealings in this Software without prior written authorization
 * of the copyright holder.
 */

/* Test program for new API available since version 3.3 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "canlock-hp.h"


/* ========================================================================== */
/* Test program for new API from version 3 */

int main(void)
{
   int res = EXIT_FAILURE;
   const char *output;
   const char input[] =
      "Date: Fri, 23 Jul 2018 20:31:47 +0200\r\n"
      "From: Foo <foo@foo.example>\r\n"
      "Message-ID: <2018-07-23%foo@foo.example>\r\n"
      "Newsgroups: foo.bar\r\n"
      "Path: server.example!.POSTED!not-for-mail\r\n"
      "Subject: Something\r\n"
      "cancel-LOCK: (Comment with quoted-pair: \\(, (Not scheme sha1:))\r\n"
      " sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=(sha1:abc)\r\n"
      "\r\n"
      "Content\r\n";
   const char expected[] =
      "cancel-LOCK: (Comment with quoted-pair: \\(, (Not scheme sha1:))"
      " sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=(sha1:abc)";

   /*
    * Return value 77 indicates that the testsuite should skip this test.
    *
    * Currently this test requires the package to be already installed.
    * For development the exit(77) can be commented out.
    * Executing the test binary from ".libs" subdirectoy should work.
    * Currently this test need the header parser binary installed
    * Comment out for development.
    *
    * Should be eventually removed after a real library implementation of the
    * header parser is available.
    */
   exit(77);

   /* Parse header */
   printf("Input for parser:\n");
   printf("-------------------------------------------------------------\n");
   printf("%s\n", input);
   printf("-------------------------------------------------------------\n");
   output = cl_hp_get_field(input, strlen(input), "Cancel-Lock");
   if (NULL == output)
      printf("Parser failed.\n");
   else
   {
      printf("\nOutput from parser  :\n");
      printf("-------------------------------------------------------------\n");
      printf("%s\n", output);
      printf("-------------------------------------------------------------\n");
      printf("\nExpected from parser:\n");
      printf("-------------------------------------------------------------\n");
      printf("%s\n", expected);
      printf("-------------------------------------------------------------\n");
      if (strcmp(output, expected))
         printf("Output not as expected.\n");
      else
         res = EXIT_SUCCESS;
      free((void *) output);
   }

   printf("\n");
   exit(res);
}


/* EOF */
