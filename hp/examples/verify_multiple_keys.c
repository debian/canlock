/* Execute authentication according to RFC 8315 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libcanlock-3/canlock.h>
#include <libcanlock-3/canlock-hp.h>


/*
 * To extract the fields from the header the function cl_hp_get_field() can
 * be used.
 *
 * The following header fields are already unfolded.
 * The function cl_hp_unfold_field() can be used if this is not the case.
 */

/* Body of the Cancel-Key header field of a cancel or supersede article */
static const char *keys =
   "Cancel-Key: sha1:xRi1jfCrgA6kCyGbk4ZUhddVdT0= (does not match) "
   "sha256:Vy+nHHZZybqxFC/uWmes153KPUpt/FoAMmMij8Ks2dM=";

/* Body of the Cancel-Lock header field of the target article */
static const char *locks =
   "Cancel-Lock: sha1:NkCyGbk4ZUhgExRi1jhCrgAVdT0= "
   "sha256:x0JAxz+Ln+ZB2pC+uPKvcka6kfK5ojebnVxEosgOujQ=";


int error_handler(const char *msg)
{
   fprintf(stderr, "Error: %s\n", msg);
   exit(EXIT_FAILURE);
}


/* Create an (initial) Cancel-Lock header field */
int main(void)
{
   char *parsed_keys, *parsed_locks;

   /* Parse header field bodies */
   parsed_keys = cl_hp_parse_field(keys, strlen(keys));
   if (NULL == parsed_keys)
      error_handler("Parser failed");
   parsed_locks = cl_hp_parse_field(locks, strlen(locks));
   if (NULL == parsed_locks)
   {
      free(parsed_keys);
      error_handler("Parser failed");
   }

   printf("Keys : %s\n", parsed_keys);
   printf("Locks: %s\n", parsed_locks);

   /*
    * Verify whether at least one key matches a lock
    *
    * No reject list will accept all hash algorithms supported by
    * libcanlock for <scheme>.
    */
   {
      int failed = cl_verify_multi(NULL, parsed_keys, parsed_locks);
      if (failed)
      {
         free(parsed_locks);
         free(parsed_keys);
         error_handler("Authentication failed");
      }
   }

   printf("Good\n");

   /* Release memory allocated by libcanlock-hp */
   free(parsed_locks);
   free(parsed_keys);

   exit(EXIT_SUCCESS);
}


/* EOF */
