#! /bin/sh

# Create a <c-lock> element with the canlock utility
# The local secret is loaded from the file "secret_512bits".

# User-ID   : testuser
# Message-ID: <ThisIsAMessageID@example.org>
#
# Note that RFC 8315 uses angle brackets like quotes in the continuous text.
# Therefore <uid> or <mid> does not mean that additional angle brackets are
# required. A User-ID must not contain angle brackets and a Message-ID always
# includes angle brackets (as defined by RFC 5536 in Section 3.1.3).
LOCK=$(canlock -a sha512 -l 'testuser<ThisIsAMessageID@example.org>' \
       <secret_512bits)


# Print (initial) Cancel-Lock header field
#
# For insertion into a message header, the canonical line termination CRLF
# must be used instead.
printf "Cancel-Lock: %s\n" "$LOCK"


# EOF
