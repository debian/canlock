# No shebang here is intended
# (use the shell that autotools configured for test-driver)

set -e

printf "%s\n\n" "Check for case-insensitive match"

EXPECTED="cancel-LOCK: (Comment with quoted-pair: \(, (Not scheme sha1:)) sha256:RrKLp7YCQc9T8HmgSbxwIDlnCDWsgy1awqtiDuhedRo=(sha1:abc)"
RESULT=$(../util/canlock-mhp -f "Cancel-Lock" <article1.txt)
if test "$RESULT" = "$EXPECTED"
then
   printf "%s\n" "$RESULT"
   exit 0
else
   printf "Error!\n"
   exit 1
fi


# EOF
