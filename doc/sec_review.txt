Security review for libcanlock 3.0.0pre27
=========================================
(2018-01-05 by Michael Baeuerle)

This review should identify places where libcanlock will copy the secret from
the caller to different memory locations. These places (marked "Lx" below) are
candidates for calling "cl_clear_secret()" internally.

Note:
A patch to overwrite locations Lx after use is included in version 3.0.0rc1.

Note2:
Since version 3.2.2 the internal API hmac* was renamed to RFC2104Hmac*.


canlock.c
=========

sha_lock() -> cl_get_lock()
                |
 sha_key() -----+-> cl_get_key() -> hmac()


hmac.c
======

hmac() -+-> hmacReset() -+-> USHAInput() -> tcontext        ; Location L2
        |                |
        |                +----------------> k_ipad[]        ; Location L3
        |
        +----------------------------------> context        ; Location L1


usha.c
======

USHAInput() -> SHA<xxx>Input()  ; With "xxx" as listed below


sha1.c
======

SHA1Input() -> SHA1ProcessMessageBlock() -> W[]             ; Location L4


sha224-256.c
============

SHA224Input() -+
               |
SHA256Input() -+-> SHA224_256ProcessMessageBlock() -> W[]   ; Location L5


sha384-512.c
============

SHA384Input() -+
               |
SHA512Input() -+-> SHA384_512ProcessMessageBlock() -> W[]   ; Location L6a/L6b


EOF
