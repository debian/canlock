# ==============================================================================
# Copyright (c) 2018-2021 Michael Baeuerle
#
# All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, and/or sell copies of the Software, and to permit persons
# to whom the Software is furnished to do so, provided that the above
# copyright notice(s) and this permission notice appear in all copies of
# the Software and that both the above copyright notice(s) and this
# permission notice appear in supporting documentation.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
# OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
# SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
# RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
# CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Except as contained in this notice, the name of a copyright holder
# shall not be used in advertising or otherwise to promote the sale, use
# or other dealings in this Software without prior written authorization
# of the copyright holder.


# ==============================================================================
# Rules for automake

ACLOCAL_AMFLAGS = -I m4
AM_CPPFLAGS = -I$(top_srcdir)/include -D_POSIX_C_SOURCE=200112L


# ==============================================================================
# Rules to build test programs

check_PROGRAMS = library_mhptest library_hfptest library_unfold \
                 library_unfold_empty library_unfold_nocolon

library_mhptest_SOURCES = library_mhptest.c
library_mhptest_LDADD = ../src/libcanlock-hp.la

library_hfptest_SOURCES = library_hfptest.c
library_hfptest_LDADD = ../src/libcanlock-hp.la

library_unfold_SOURCES = library_unfold.c
library_unfold_LDADD = ../src/libcanlock-hp.la

library_unfold_empty_SOURCES = library_unfold_empty.c
library_unfold_empty_LDADD = ../src/libcanlock-hp.la

library_unfold_nocolon_SOURCES = library_unfold_nocolon.c
library_unfold_nocolon_LDADD = ../src/libcanlock-hp.la


# ==============================================================================
# Executables to run for "make check"

TESTS = mhptest1.sh mhptest2.sh mhptest3.sh mhptest4.sh mhptest5.sh \
        hfptest1.sh hfptest2.sh hfptest_obs.sh \
        library_mhptest library_hfptest library_unfold \
        library_unfold_empty library_unfold_nocolon

XFAIL_TESTS = library_unfold_empty library_unfold_nocolon


# EOF
