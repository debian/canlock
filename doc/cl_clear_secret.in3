.\"=============================================================================
.\" Header
.\"
.\" Copyright (c) 2018-2021 Michael Baeuerle
.\"
.\" All rights reserved.
.\"
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, and/or sell copies of the Software, and to permit persons
.\" to whom the Software is furnished to do so, provided that the above
.\" copyright notice(s) and this permission notice appear in all copies of
.\" the Software and that both the above copyright notice(s) and this
.\" permission notice appear in supporting documentation.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
.\" EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
.\" OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
.\"  HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY
.\" SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER
.\" RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
.\" CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.\" Except as contained in this notice, the name of a copyright holder
.\" shall not be used in advertising or otherwise to promote the sale, use
.\" or other dealings in this Software without prior written authorization
.\" of the copyright holder.
.\"
.TH $NAME 3 $DATE Unix "$NAME $VERSION manual"
.\"
.\"
.\"=============================================================================
.\" NAME section
.\"
.SH NAME
cl_clear_secret - Overwrite secret data in memory with $NAME
.\"
.\"
.\"=============================================================================
.\" SYNOPSIS section
.\"
.SH SYNOPSIS
.nf
.B #include <libcanlock-3/canlock.h>
.sp
.BI "int cl_clear_secret(void *" sec ", size_t " sec_size ,
.BI "                    size_t " buf_size );
.\"
.\"
.\"=============================================================================
.\" DESCRIPTION section
.\"
.SH DESCRIPTION
The
.BR cl_clear_secret ()
function tries to overwrite
.I sec_size
bytes of memory starting at the address specified by the
.I sec
parameter.
.sp
The size
.I buf_size
must be set to the size of the whole buffer.
.sp
If the operating system provides
.BR memset_s (),
it is called with both length values. Otherwise nonportable functions like
.BR explicit_memset ()
are used if available.
.br
If neither
.BR memset_s ()
nor a nonportable replacement is available, a call to
.BR memset ()
is used instead, but the compiler maybe optimize this attempt to NOP.
A positive value is returned as warning.
.\"
.\"
.\"=============================================================================
.\" RETURN VALUE section
.\"
.SH RETURN VALUE
Upon successful completion zero is returned.
.br
Negative values indicate an error.
.br
Positive values indicate a warning.
.sp
The value -1 is returned if the parameter
.I sec
is NULL or if
.I sec_size
is greater than
.IR buf_size .
.sp
The value 1 indicates missing support for explicit memory access from the
operating system.
.\"
.\"
.\"=============================================================================
.\" AUTHORS section
.\"
.SH AUTHORS
Michael Baeuerle
.\"
.\"
.\"=============================================================================
.\" REPORTING BUGS section
.\"
.SH REPORTING BUGS
Report bugs to <mailto:$BUGREPORT>.
.\"
.\"
.\"=============================================================================
.\" STANDARDS section
.\"
.SH STANDARDS
.B $NAME
tries to comply with the following standards:
.PP
RFC 5537, RFC 6234, RFC 8315
.\"
.\"
.\"=============================================================================
.\" SEE ALSO section
.\"
.SH SEE ALSO
.BR cl_get_key (3),
.BR cl_get_lock (3),
.BR cl_split (3),
.BR cl_verify (3),
.BR cl_verify_multi (3),
.BR canlock (1),
.BR memset (3),
.BR memset_s (3)
.\"
.\"
.\" EOF
